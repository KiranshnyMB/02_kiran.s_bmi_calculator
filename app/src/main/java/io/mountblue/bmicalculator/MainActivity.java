package io.mountblue.bmicalculator;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private RelativeLayout mKeyboardLayout;
    private RelativeLayout mAnswerFragmentLayout;
    private TextView mHeightValueView;
    private TextView mWeightValueView;
    private TextView mCurrentValueView;
    private TextView mHeightUnitView;
    private TextView mWeightUnitView;
    private TextView mBmiResultView;
    private TextView mWeightStatusView;
    private ImageView mBackButtonImageView;
    private Toast mToast;

    private final String RESULT_VALUE_KEY = "resultValue";
    private final String HEIGHT_VALUE_KEY = "heightValue";
    private final String WEIGHT_VALUE_KEY = "weightValue";
    private final String HEIGHT_UNIT_VALUE_KEY = "heightUnitValue";
    private final String WEIGHT_UNIT_VALUE_KEY = "heightUnitValue";
    private final String HEIGHT_VALUE_COLOR_KEY = "heightValueColor";
    private final String WEIGHT_VALUE_COLOR_KEY = "weightValueColor";
    private final String KEYBOARD_VISIBILITY_KEY = "keyboardVisibility";
    private final String RESULT_SCREEN_VISIBILITY_KEY = "resultScreenVisibility";
    private final String CURRENT_SELECTED_VIEW_KEY = "currentSelectedValueView";
    private final String HEIGHT_KEY = "height";
    private final String WEIGHT_KEY = "weight";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing views
        Spinner mWeightSpinner = findViewById(R.id.weightSpinner);
        Spinner mHeightSpinner = findViewById(R.id.heightSpinner);
        mWeightSpinner.setOnItemSelectedListener(this);
        mHeightSpinner.setOnItemSelectedListener(this);
        mKeyboardLayout = findViewById(R.id.keyboard);
        mAnswerFragmentLayout = findViewById(R.id.result_fragment);
        mHeightValueView = findViewById(R.id.heightValueTextView);
        mWeightValueView = mCurrentValueView = findViewById(R.id.weightValueTextView);
        mWeightUnitView = findViewById(R.id.weightUnits);
        mHeightUnitView = findViewById(R.id.heightUnits);
        mBmiResultView = findViewById(R.id.bmi_result);
        mWeightStatusView = findViewById(R.id.weight_status);
        mBackButtonImageView = findViewById(R.id.back_button);

        showKeyboard(mCurrentValueView);

        //whenever screen is rotated
        if (savedInstanceState != null) {
            mBmiResultView.setText(savedInstanceState.getString(RESULT_VALUE_KEY));
            mHeightValueView.setText(savedInstanceState.getString(HEIGHT_VALUE_KEY));
            mWeightValueView.setText(savedInstanceState.getString(WEIGHT_VALUE_KEY));
            mHeightUnitView.setText(savedInstanceState.getString(HEIGHT_UNIT_VALUE_KEY));
            mWeightUnitView.setText(savedInstanceState.getString(WEIGHT_UNIT_VALUE_KEY));
            mHeightValueView.setTextColor(savedInstanceState.getInt(HEIGHT_VALUE_COLOR_KEY));
            mWeightValueView.setTextColor(savedInstanceState.getInt(WEIGHT_VALUE_COLOR_KEY));
            mKeyboardLayout.setVisibility(savedInstanceState.getInt(KEYBOARD_VISIBILITY_KEY));
            mAnswerFragmentLayout.setVisibility(savedInstanceState.getInt(RESULT_SCREEN_VISIBILITY_KEY));
            if (Objects.requireNonNull(savedInstanceState.getString(CURRENT_SELECTED_VIEW_KEY)).equals(HEIGHT_KEY)) {
                mCurrentValueView = mHeightValueView;
            } else if (Objects.requireNonNull(savedInstanceState.getString(CURRENT_SELECTED_VIEW_KEY)).equals(WEIGHT_KEY)){
                mCurrentValueView = mWeightValueView;
            }
        }

        //to show notification bar Tint
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(KEYBOARD_VISIBILITY_KEY, mKeyboardLayout.getVisibility());
        savedInstanceState.putInt(RESULT_SCREEN_VISIBILITY_KEY, mAnswerFragmentLayout.getVisibility());
        savedInstanceState.putString(RESULT_VALUE_KEY, mBmiResultView.getText().toString());
        savedInstanceState.putString(HEIGHT_VALUE_KEY, mHeightValueView.getText().toString());
        savedInstanceState.putString(WEIGHT_VALUE_KEY, mWeightValueView.getText().toString());
        savedInstanceState.putString(HEIGHT_UNIT_VALUE_KEY, mHeightUnitView.getText().toString());
        savedInstanceState.putString(WEIGHT_UNIT_VALUE_KEY, mWeightUnitView.getText().toString());
        savedInstanceState.putInt(HEIGHT_VALUE_COLOR_KEY, mHeightValueView.getCurrentTextColor());
        savedInstanceState.putInt(WEIGHT_VALUE_COLOR_KEY, mWeightValueView.getCurrentTextColor());
        String currentViewValue = "";
        if (mHeightValueView.getCurrentTextColor() == getResources().getColor(R.color.colorACButtonText)) {
            currentViewValue += HEIGHT_KEY;
        } else {
            currentViewValue += WEIGHT_KEY;
        }
        savedInstanceState.putString(CURRENT_SELECTED_VIEW_KEY, currentViewValue);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onButtonClick(View view) {
        String defaultNumber = "0";
        String decimalSeparator = ".";
        switch (view.getId()) {
            case R.id.buttonClear:
                String currentValue = mCurrentValueView.getText().toString();
                if (!currentValue.equals(defaultNumber)) {
                    if (currentValue.length() != 1) {
                        currentValue = currentValue.substring(0, currentValue.length() - 1);
                        mCurrentValueView.setText(currentValue);
                    } else {
                        mCurrentValueView.setText(defaultNumber);
                    }
                }
                break;
            case R.id.buttonGo:
                String weight = mWeightValueView.getText().toString();
                String height = mHeightValueView.getText().toString();
                if (!(Double.valueOf(weight) == 0)
                        || !(Double.valueOf(height) == 0))
                    calculateBmi(height, weight);
                else {
                    if (mToast != null) {
                        mToast.cancel();
                    }
                    mToast = Toast.makeText(this, getString(R.string.warning_message), Toast.LENGTH_LONG);
                    mToast.show();
                }
                break;

            case R.id.buttonAC:
                mCurrentValueView.setText(defaultNumber);
                break;

            case R.id.buttonDot: {
                String currentValueForDotAppend = mCurrentValueView.getText().toString();
                if (!currentValueForDotAppend.contains(decimalSeparator)) {
                    if (!(currentValueForDotAppend.length() > 2)) {
                        mCurrentValueView.setText(String.format("%s%s", mCurrentValueView.getText().toString(),
                                ((Button) view).getText().toString()));
                    }
                }
                break;
            }

            default:
                String currentValueForAddition = mCurrentValueView.getText().toString();
                if (!currentValueForAddition.equals(defaultNumber)) {
                    if (!(currentValueForAddition.length() > 2)) {
                        mCurrentValueView.setText(String.format("%s%s", mCurrentValueView.getText().toString(), ((Button) view).getText().toString()));
                    } else if (currentValueForAddition.contains(decimalSeparator)) {
                        if (!(currentValueForAddition.length() > 5)) {
                            mCurrentValueView.setText(String.format("%s%s", mCurrentValueView.getText().toString(), ((Button) view).getText().toString()));
                        }
                    }
                } else {
                    mCurrentValueView.setText(((Button) view).getText().toString());
                }
                break;
        }
    }


    private void calculateBmi(String heightString, String weightString) {
        int maxBmi = 49;
        int minBmi = 0;

        double minNormalBmi = 18.5;
        double minOverWeightBmi = 25.0;
        double weight = Double.parseDouble(weightString);
        double height = Double.valueOf(heightString);
        double bmi;

        String weightUnits = mWeightUnitView.getText().toString();
        String heightUnits = mHeightUnitView.getText().toString();
        String weightStatusString = "";

        int color = 0;

        DecimalFormat numberFormat = new DecimalFormat("#.0");

        //weight conversion
        if (weightUnits.equals("Pound")) {
            weight = weight / 2.205;
        }

        //height conversion
        switch (heightUnits) {
            case "Centimeter":
                height = height / 100;
                break;
            case "Feet":
                height = height / 3.281;
                break;
            case "Inch":
                height = height / 39.37;
                break;
        }

        //bmi formula
        bmi = weight / (height * height);

        if (bmi > maxBmi || bmi < minBmi) {
            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(this, getString(R.string.warning_message), Toast.LENGTH_LONG);
            mToast.show();
            return;
        }

        if (bmi >= minOverWeightBmi) {
            weightStatusString = getResources().getString(R.string.overweight);
            color = getResources().getColor(R.color.overWeight_color);
        } else if (bmi >= minNormalBmi) {
            weightStatusString = getResources().getString(R.string.normal);
            color = getResources().getColor(R.color.normalWeight_color);
        } else if (bmi >= minBmi) {
            weightStatusString = getResources().getString(R.string.underweight);
            color = getResources().getColor(R.color.underweight_color);
        }

        mBmiResultView.setText(numberFormat.format(bmi));
        mWeightStatusView.setText(weightStatusString);
        mWeightStatusView.setTextColor(color);

        mBackButtonImageView.setVisibility(View.VISIBLE);
        mKeyboardLayout.setVisibility(View.GONE);
        mAnswerFragmentLayout.setVisibility(View.VISIBLE);
    }

    public void onValueButtonPress(View view) {
        if (view.getId() == R.id.weightValueTextView) {
            mWeightValueView.setTextColor(getResources().getColor(R.color.valueClicked));
            mHeightValueView.setTextColor(getResources().getColor(R.color.valueUnClicked));
            mCurrentValueView = (TextView) view;
            showKeyboard(view);
        } else if (view.getId() == R.id.heightValueTextView) {
            mHeightValueView.setTextColor(getResources().getColor(R.color.valueClicked));
            mWeightValueView.setTextColor(getResources().getColor(R.color.valueUnClicked));
            mCurrentValueView = (TextView) view;
            showKeyboard(view);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        if (parentView.getId() == R.id.weightSpinner) {
            mWeightUnitView.setText(parentView.getItemAtPosition(position).toString());
            showKeyboard(parentView);
        } else if (parentView.getId() == R.id.heightSpinner) {
            mHeightUnitView.setText(parentView.getItemAtPosition(position).toString());
            showKeyboard(parentView);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parentView) {
        showKeyboard(parentView);
    }

    public void showKeyboard(View view) {
        mBackButtonImageView.setVisibility(View.INVISIBLE);
        mKeyboardLayout.setVisibility(View.VISIBLE);
        mAnswerFragmentLayout.setVisibility(View.GONE);
    }
}
